module Main where

import Board
import GameTree
import Data.Maybe
import Data.List
import System.IO


-- odpalając w ghci warto zostawić stosunkowo mały rozmiar planszy, przy budowaniu ze stackiem można ustawić już 19x19
boardSize = 15

insideBoard :: Pos -> Bool
insideBoard (x,y) 
    | x < boardSize && y < boardSize && x >= 0 && y >= 0 = True
    | otherwise = False

parseInput :: String -> Maybe Pos
parseInput line
    | areTwo tab && isInteger xs && isInteger ys = Just (x,y)
    | otherwise = Nothing
    where
        x = (read xs :: Int) - 1
        y = (read ys :: Int) - 1
        xs = head tab
        ys = head $ tail tab    
        tab = words line
        areTwo tab = length tab > 1

isInteger :: String -> Bool
isInteger s = case reads s :: [(Integer, String)] of
  [(_, "")] -> True
  _         -> False

escapeSeqence :: String -> Bool
escapeSeqence line = ":q" `isInfixOf` line

escape :: IO ()
escape = putStrLn "Gomoku quit"

main :: IO ()
main = do 
    hSetBuffering stdout NoBuffering 
    gameLoop $ initBoard boardSize


aiTurn :: Board -> Pos -> IO ()
aiTurn board@(Board matrix player) lastpos = do
    print board
    putStrLn "(AI move)"
    let newBoard = getNextMove $ generateTree O 2 lastpos board
    if checkWin O newBoard then do
        print newBoard
        putStrLn "You lose!"
        main
    else gameLoop newBoard


gameLoop :: Board -> IO ()
gameLoop board@(Board matrix player) = do
    print board
    putStr "(Your move) "
    putStr "Insert row and col: "
    line <- getLine
    if escapeSeqence line then escape
    else do
        let justpos = parseInput line
        if isJust justpos then  do
            let pos = fromJust justpos
            if insideBoard pos then 
                if isEmpty pos board then do
                    let newBoard = putSymbol X pos board 
                    if checkWin X newBoard then do
                        print newBoard
                        putStrLn "You win!"
                        main
                    else aiTurn newBoard pos
                else do
                    putStrLn "position taken, choose another one."            
                    gameLoop board
            else do
                putStrLn "outside the board, choose correct"
                gameLoop board
        else do 
            putStrLn "insert correct coords."            
            gameLoop board
