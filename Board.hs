module Board where 

import Data.Char
import Data.List
import Data.Maybe

data Board  = Board {rows :: [[Symbol]], lastPos :: Pos}

data Symbol = X | O | Empty deriving Eq

type Pos = (Int, Int)

data Player = Human | AI

instance Show Symbol where
    show Empty = "."
    show X = "x"
    show O = "o"

instance Show Board where
     show (Board x _) = unlines $ map (unwords . map show) x

initBoard :: Int -> Board
initBoard n = Board (replicate n (replicate n Empty)) (0,0)

isEmpty :: Pos -> Board -> Bool
isEmpty (x,y) board = ((rows board !! x) !! y) == Empty

takeAt :: Pos -> Board -> Symbol
takeAt (x,y) board = (rows board !! x) !! y

putSymbol :: Symbol -> Pos -> Board -> Board
putSymbol symbol pos@(row, col) board@(Board array p)
    | isEmpty pos board = Board (y ++ (xs:ys)) p 
    | otherwise = board
    where
        (y, r:ys) = splitAt row array
        xs = insertIntoRow symbol col r

insertIntoRow :: Symbol -> Int -> [Symbol] -> [Symbol]
insertIntoRow s idx row = a ++ (s:b) where
    (a, _ : b) = splitAt idx row

nextSymbol :: Symbol -> Symbol
nextSymbol X = O
nextSymbol O = X    


-- generowanie wszystkich mozliwych następnych ruchów
possibleMoves :: Board -> [Pos]
possibleMoves board@(Board matrix _) = [(x, y) | x <- [0..(size - 1)], 
                                                      y <- [0..(size - 1)], 
                                                      isEmpty (x,y) board]
                                                      where size = length matrix

-- generowanie ruchów w ograniczonym obszarze 
smarterMoves :: Board -> Pos -> [Pos]
smarterMoves board (oldX,oldY) = [(x, y) | x <- listX, 
                                            y <- listY, 
                                            isEmpty (x,y) board]
                                            where 
                                                listX = goLeft ++ goRight
                                                listY = goUp ++ goDown
                                                goLeft = filter (>= 0) [oldX - x | x <- moves]
                                                goRight = filter (<= size - 1) [oldX + x | x <- moves]
                                                goUp = filter (>= 0) [oldY - y | y <- moves]
                                                goDown = filter (<= size - 1) [oldY + y | y <- moves]
                                                size = length $ rows board
                                                moves = [1..8]

-- generowanie kolejnych stanów gry
nextStates :: Board -> Pos -> Symbol -> [Board]
nextStates board oldpos symbol = [putSymbol symbol pos board | pos <- smarterMoves board (lastPos board)]
-- nextStates board oldpos symbol = [putSymbol symbol pos board | pos <- possibleMoves board]
