module GameTree where

import Board
import Data.Char
import Data.List
import Data.Maybe

data GameTree = Node {nodeBoard :: Board, nextMoves :: [GameTree], sym :: Symbol} deriving Show


-- generowanie drzewa oraz minimax

generateTree :: Symbol -> Int -> Pos -> Board -> GameTree
generateTree s 0 _ board  = Node board [] s
generateTree symbol depth lastpos board = Node board (map (generateTree (nextSymbol symbol) (depth - 1) (lastPos board)) (nextStates board (lastPos board) symbol)) symbol

minmax :: GameTree -> Int
minmax (Node board [] _) = rateBoard board
minmax (Node board list O) = maximum (map minmax list)
minmax (Node board list X) = minimum (map minmax list) 

getNextMove :: GameTree -> Board
getNextMove board@(Node _ list _) = nodeBoard $ fst $ foldl bestOf (head list, minmax $ head list) list

bestOf :: (GameTree, Int) -> GameTree -> (GameTree, Int)
bestOf acc@(tree, val) b 
    | val > valNext = acc
    | otherwise = (b, valNext)
    where
        valNext = minmax b


-- ewaluacja planszy 

allFives :: Board -> [[Symbol]]
allFives (Board matrix _) = concatMap takeFive (matrix ++ transpose matrix ++ diagonals matrix)

takeFive :: [Symbol] -> [[Symbol]]
takeFive list@(x:xs)
    | length list >= 5 && moreThanTwo five = five : takeFive xs 
    | otherwise = []
    where 
        five = take 5 list
        moreThanTwo x = length (filter (== Empty) x) < 4

symbolEqTo :: (Eq a) => a -> [a] -> Bool
symbolEqTo symbol = all (== symbol)

checkWin :: Symbol -> Board -> Bool
checkWin symbol board@(Board list _) = any (symbolEqTo symbol) (allFives board)

diagonals :: [[a]] -> [[a]]
diagonals matrix = oneDiag matrix ++ oneDiag (reverse matrix)
-- diagonals matrix = oneDiag matrix

oneDiag :: [[a]] -> [[a]]
oneDiag = tail . go [] where
    go b es_ = [h | h:_ <- b] : case es_ of
        []   -> transpose ts
        e:es -> go (e:ts) es
        where ts = [t | _:t <- b]

rateBoard :: Board -> Int
rateBoard board
    | checkWin O board = 100000
    | checkWin X board = -100000
    | otherwise = foldr (\x acc -> acc + rateFive x) 0 (allFives board)

rateFive :: [Symbol] -> Int
rateFive five = fourO + threeO + twoO - fourX - threeX - twoX
    where
        fourX = 2000 * occurencesOf [[Empty,X,X,X,X], [X,Empty,X,X,X], [X,X,Empty, X,X]]
        fourO = 1001 * occurencesOf [[Empty,O,O,O,O], [O,Empty,O,O,O], [O,O,Empty, O,O], [X,O,O,O,O]]
        threeX = 400 * occurencesOf [[Empty,Empty,X,X,X], [X,Empty,Empty,X,X],[X,Empty,X,X,Empty],[X,X,Empty,X,Empty]]
        threeO = 50 * occurencesOf [[Empty,Empty,O,O,O], [O,Empty,Empty,O,O],[O,Empty,O,O,Empty],[O,O,Empty,O,Empty]]
        twoX = 20 * occurencesOf [[X,X,Empty,Empty,Empty], [Empty, X, Empty, X, Empty], [Empty, Empty, X, Empty, X]]
        twoO = 10 * occurencesOf [[O,O,Empty,Empty,Empty], [Empty, O, Empty, O, Empty], [Empty, Empty, O, Empty, O]]
        occurencesOf patterns = length $ filter (`isInfixOf` five) (patterns ++ map reverse patterns)

