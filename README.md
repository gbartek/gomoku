# Gomoku #

Gomoku with minmax algorithm written in Haskell. 

### Compile & play ###

* Compile:
```
# ghc --make Main.hs
```
* Run:
```
./Main
```
* Play!

* To quit type `:q` when it's your turn to make a move.

### build with Stack: ###

* Checkout to branch named 'stack'. 

* Build with some optimizing options:
```
# stack build --executable-profiling --library-profiling --ghc-options="-fprof-auto -rtsopts"
```
* Exec (e.g. under Windows):
```
# stack exec gom.exe --RTS -- -flagBefore +RTS -p -RTS -flagAfter
```